import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

axios.defaults.withCredentials = true
axios.defaults.baseURL = 'http://local-vsanc-backend-api.com'
// axios.defaults.baseURL = 'http://local-vsanc-backend-api.com/api'
// axios.defaults.baseURL = 'http://localhost:8080'

console.log('axios::')
console.log(axios)


Vue.config.productionTip = false


new Vue({
  render: h => h(App),
}).$mount('#app')
